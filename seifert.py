import pyvoxelize as pv
import numpy as np

# The polynomials
def seifert_base(X, Y, Z):
    D = 4+X**2+Y**2+Z**2
    x = 4*X/D
    y = 4*Y/D
    z = 4*Z/D
    t = 1 - 8/D
    
    rho = x**2 + y**2 + (z**2 + t**2)**2
    f1 = ((z**2-t**2)*rho + x*(8*x**2-2*rho))  *D**6
    f2 = (2*np.sqrt(2)*x*z*t + y*(8*x**2-rho)) *D**6 #double check coeff of rho

    return f1, f2

p1, p2 = pv.get_approximate_coefficients(seifert_base)

m = 64
theta = -np.pi + 2*np.pi/m*np.arange(m).reshape(-1,1,1,1)
Q1 = np.cos(theta)*p1 + np.sin(theta)*p2
Q2 = np.sin(theta)*p1 - np.cos(theta)*p2

# The plot
fig = pv.Plot3D(lower=-6, upper=7, show=False)

fig.curve(p1, p2, always_show=True)
fig.surface_batch([dict(eqs=Q1[i], ineqs=Q2[i]) for i in range(m)], verbose=1)

fig.show()
fig.animate()

fig.keep_open()

