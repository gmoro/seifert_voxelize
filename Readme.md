# Visualisation of the Seifert node using `voxelize`

Install voxelize with:

    pip install git+https://gitlab.inria.fr/gmoro/voxelize.git

Then run the python file with:

    python seifert.py

Or visualize the jupyter document `Seifert.ipynb` with:

    jupyter notebook
